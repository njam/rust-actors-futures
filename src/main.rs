#![feature(async_await)]

use std::future::Future;
use std::pin::Pin;
use std::thread;
use std::time::Duration;

use futures::FutureExt;
use signal_hook::iterator::Signals;

use actor::*;

mod actor;


struct Player { id: u64 }

struct Ping { count: u64, addr: ActorAddr<Player> }

impl ActorReceive<Ping> for Player {
    fn receive(&mut self, msg: Ping, context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()>>> {
        let addr_self = context.addr();
        println!("Player {} received: {}", self.id, msg.count);
        futures_timer::Delay::new(Duration::from_millis(500))
            .then(move |_| {
                if msg.count < 10 {
                    msg.addr.send_ok(Ping { count: msg.count + 1, addr: addr_self });
                }
                futures::future::ready(())
            })
            .boxed()
    }
}


fn main() {
    let mut executor = futures::executor::LocalPool::new();
    let mut scheduler = ActorScheduler::new(executor.spawner());

    let addr1 = scheduler.spawn(Player { id: 1 });
    let addr2 = scheduler.spawn(Player { id: 2 });

    addr2.send_ok(Ping { count: 1, addr: addr1.clone() });

    thread::spawn(move || {
        let signals = Signals::new(&[signal_hook::SIGTERM, signal_hook::SIGINT, signal_hook::SIGQUIT]).unwrap();
        for _signal in signals.forever() {
            addr1.terminate();
            addr2.terminate();
        }
    });
    executor.run();
}
