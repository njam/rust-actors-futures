use std::future::Future;
use std::marker::PhantomData;
use std::pin::Pin;

use futures::channel::mpsc;
use futures::StreamExt;
use futures::task::LocalSpawnExt;
use std::fmt;
use std::error::Error;

/// The scheduler creates futures to process actor messages, and spawns them on a future spawner.
/// Currently it spawns on a `dyn LocalSpawn`, but probably spawning on a `dyn Spawn` would be useful too.

pub struct ActorScheduler {
    spawner: Box<dyn futures::task::LocalSpawn>
}

impl ActorScheduler {
    pub fn new<S>(spawner: S) -> Self
        where S: futures::task::LocalSpawn + 'static
    {
        Self {
            spawner: Box::new(spawner)
        }
    }

    pub fn spawn<A>(&mut self, mut actor: A) -> ActorAddr<A>
        where A: 'static
    {
        let (tx, rx) = actor_channel();
        let mut context = ActorExecutionContext { tx: tx.clone() };
        let future = rx.stream()
            .take_while(|envelope| {
                match &envelope.payload {
                    EnvelopePayload::Terminate =>
                        futures::future::ready(false),
                    _ =>
                        futures::future::ready(true),
                }
            })
            .for_each(move |envelope| {
                match envelope.payload {
                    EnvelopePayload::Message(data) =>
                        futures::future::Either::Left(
                            data.handle_with_actor(&mut actor, &mut context)
                        ),
                    EnvelopePayload::Terminate =>
                        futures::future::Either::Right(
                            futures::future::ready(())
                        ),
                }
            });
        self.spawner.spawn_local(future).unwrap();
        tx
    }
}


/// Trait to implement on actors for receiving messages.

pub trait ActorReceive<M>
    where
        Self: Sized,
{
    fn receive(&mut self, msg: M, context: &mut ActorExecutionContext<Self>) -> Pin<Box<dyn Future<Output=()>>>;
}


/// Context passed to `receive()` for accessing the actor's own address.

pub struct ActorExecutionContext<A> where {
    tx: ActorAddr<A>,
}

impl<A> ActorExecutionContext<A> where {
    pub fn addr(&self) -> ActorAddr<A> {
        self.tx.clone()
    }
}


/// Wrapper around an actor message, generic over the actor `A`.
/// Contains a boxed trait object, generic over the message type `M`.

struct Envelope<A> {
    pub payload: EnvelopePayload<A>,
}

enum EnvelopePayload<A> {
    Message(Box<dyn EnvelopeMessageTrait<Actor=A>>),
    Terminate,
}

impl<A> Envelope<A> {
    fn new_message<M>(msg: M) -> Self
        where
            M: Send + 'static,
            A: ActorReceive<M> + 'static
    {
        let payload = EnvelopePayload::Message(Box::new(EnvelopeMessage {
            msg,
            actor_phantom: PhantomData,
        }));
        Envelope { payload }
    }

    fn new_terminate() -> Self {
        let payload = EnvelopePayload::Terminate;
        Envelope { payload }
    }
}


struct EnvelopeMessage<A, M>
    where
        M: Send,
{
    actor_phantom: PhantomData<A>,
    msg: M,
}

// Unsafely marking `EnvelopeMessage` as `Send`, because the only non-`Send` field is phantom data.
unsafe impl<A, M> Send for EnvelopeMessage<A, M>
    where
        M: Send,
{}

trait EnvelopeMessageTrait: Send {
    type Actor;

    fn handle_with_actor(self: Box<Self>, actor: &mut Self::Actor, context: &mut ActorExecutionContext<Self::Actor>) -> Pin<Box<dyn Future<Output=()>>>;
}

impl<A, M> EnvelopeMessageTrait for EnvelopeMessage<A, M>
    where
        A: ActorReceive<M>,
        M: Send,
{
    type Actor = A;

    fn handle_with_actor(self: Box<Self>, actor: &mut Self::Actor, context: &mut ActorExecutionContext<Self::Actor>) -> Pin<Box<dyn Future<Output=()>>> {
        <Self::Actor as ActorReceive<M>>::receive(actor, self.msg, context)
    }
}


/// Channel for sending and receiving actor messages.

fn actor_channel<A>() -> ActorChannel<A> {
    let (tx, rx) = mpsc::unbounded();
    (ActorAddr { tx }, ActorRecv { rx })
}

type ActorChannel<A> = (
    ActorAddr<A>,
    ActorRecv<A>,
);

pub struct ActorAddr<A> {
    tx: mpsc::UnboundedSender<Envelope<A>>,
}

impl<A> ActorAddr<A> {
    pub fn send<M>(&self, message: M) -> Result<(), ActorSendError>
        where
            M: Send + 'static,
            A: ActorReceive<M> + 'static,
    {
        self.send_envelope(Envelope::new_message(message))
    }

    pub fn send_ok<M>(&self, message: M)
        where
            M: Send + 'static,
            A: ActorReceive<M> + 'static,
    {
        self.send(message).ok();
    }

    pub fn terminate(&self) {
        self.send_envelope(Envelope::new_terminate())
            .expect("Cannot send 'termination' message.");
    }

    pub fn clone(&self) -> Self {
        Self { tx: self.tx.clone() }
    }

    fn send_envelope(&self, envelope: Envelope<A>) -> Result<(), ActorSendError> {
        self.tx.unbounded_send(envelope)
            .map_err(|e| ActorSendError { source: e.into_send_error() })
    }
}

#[derive(Debug)]
pub struct ActorSendError {
    source: mpsc::SendError
}

impl fmt::Display for ActorSendError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Cannot send message to actor: {}", self.source)
    }
}

impl Error for ActorSendError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.source)
    }
}

pub struct ActorRecv<A> {
    rx: mpsc::UnboundedReceiver<Envelope<A>>,
}

impl<A> ActorRecv<A> {
    fn stream(self) -> impl futures::Stream<Item=Envelope<A>> {
        self.rx
    }
}
